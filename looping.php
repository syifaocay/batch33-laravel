<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=, initial-scale=1.0">
  <title>Document</title>
</head>

<body>
  <h1>contoh soal looping</h1>

  <?php
  echo "<h3>jawab soal 1</h3>";
  echo "<h5>looping 1</h5>";

  for ($i = 1; $i <= 19; $i += 2) {
    echo $i . " - i love PHP <br/>";
  }

  echo "<br/><br/><br/>";

  echo "<h5>looping 2</h5>";

  $x = 19;
  do {
    echo $x . " - i love PHP <br/>";
    $x -= 2;
  } while ($x >= 1);

  echo "<br> <br/>";

  echo "<h3>jawab soal 2</h3>";
  $nomor = [18, 45, 29, 61, 47, 34];
  echo "array number : ";
  print_r($nomor);

  echo "<br>";

  foreach ($nomor as $value) {
    $rest[] = $value %= 5;
  }

  echo "array number sisa hasil bagi 5 : ";
  print_r($rest);

  echo "<br> <br/>";


  echo "<h3>jawab soal 3</h3>";
  $strainer = [
    [001, "keyboard logitel", 60000, "keyboard yang mantap untuk kantoran", "logitek.jpg"],
    [002, "Keyboard MSI", 30000, "Keyboard gaming MSI mekanik", "msi.jpeg"],
    [003, "Mouse Genius", 50000, "Mouse Genius biar lebih pinter", "genius.jpeg"],
    [004, "Mouse Jerry", 30000, "Mouse yang disukai kucing", "mouse.jpg"],
  ];

  foreach ($strainer as $value) {
    $tampung = [
      'id' => $value[0],
      'name' => $value[1],
      'price' => $value[2],
      'description' => $value[3],
      'source' => $value[4],

    ];
    print_r($tampung);
    echo "<br>";
  }

  ?>
</body>

</html>